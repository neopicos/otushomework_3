namespace DataAccess.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}