using Core.Domain.Administration;
using Core.Domain.PromoCodeManagement;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public sealed class AppDbContext : DbContext
    {
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }
    }
}