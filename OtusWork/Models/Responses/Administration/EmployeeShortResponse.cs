using System;
using System.Collections.Generic;
using Core.Domain.Administration;

namespace OtusWork.Models
{
    public class EmployeeShortResponse
    {
        public Guid Id { get; set; }
        
        public string FullName { get; set; }

        public string Email { get; set; }

    }
}