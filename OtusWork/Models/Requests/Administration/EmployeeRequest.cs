using System;

namespace OtusWork.Models.Requests
{
    public class EmployeeRequest
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int NumPromoCodeApplied { get; set; }
        public Guid RoleId { get; set; }
    }
}