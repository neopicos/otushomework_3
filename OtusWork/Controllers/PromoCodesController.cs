﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Abstractions;
using Core.Domain.PromoCodeManagement;
using OtusWork.Models.Requests.PromoCodeManagment;
using OtusWork.Models.Responses.PromoCodeManagment;

namespace OtusWork.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PromoCodesController : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromoCodesController(IRepository<PromoCode> promoCodesRepository,
                                    IRepository<Preference> preferencesRepository,
                                    IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }
        
        /// <summary>
        /// Получить список вех промокодов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<PromoCodeShortResponse>> GetPromoCodesAsync()
        {
            var promoCodes = await _promoCodesRepository.GetAllAsync();

            var response = promoCodes.Select(x => new PromoCodeShortResponse()
            {
                 Id = x.Id,
                 Code = x.Code,
                 ServiceInfo = x.ServiceInfo,
                 BeginDate = x.BeginDate.ToString("yyyy-M-d"),
                 EndDate = x.EndDate.ToString("yyyy-M-d"),
                 PartnerName = x.PartnerName
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        [HttpPost]
        public void GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            
        }

    }
}
