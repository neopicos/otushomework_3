using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Core.Abstractions;
using Core.Domain.PromoCodeManagement;
using Microsoft.AspNetCore.Mvc;
using OtusWork.Models.Requests.PromoCodeManagment;
using OtusWork.Models.Responses.PromoCodeManagment;

namespace OtusWork.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v2/[controller]")]
    public class CustomersController : Controller
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository,
                                   IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        
        /// <summary>
        /// Получить список всех клиентов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var responseList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email
                }).ToList();

            return Ok(responseList);
        }
        
        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync([Required] Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            var responseObj = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.Preferences.Select(x => new PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList()
            };

            return Ok(responseObj);
        }
        
        /// <summary>
        /// Удалить клиента по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<HttpStatusCode> DeleteCustomerAsync([Required] Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            await _customerRepository.DeleteAsync(customer);

            return HttpStatusCode.OK;
        }
        
        /// <summary>
        /// Создать нового клиента
        /// </summary>
        [HttpPost]
        public async Task<HttpStatusCode> CreateCustomerAsync(CustomerRequest customerRequest)
        {
            if (!ModelState.IsValid)
                return HttpStatusCode.BadRequest;

            var preferences = await _preferenceRepository.GetAllAsync();
            var prefList = customerRequest.PreferenceIds.Select(prefGuid =>
                    preferences.SingleOrDefault(x => x.Id == prefGuid)).ToList();

            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = customerRequest.FirstName,
                LastName = customerRequest.LastName,
                Email = customerRequest.Email,
                Preferences = prefList
            };

            await _customerRepository.AddAsync(customer);

            return HttpStatusCode.OK;
        }
        
        /// <summary>
        /// Изменить данные существующего клиента
        /// </summary>
        [HttpPut("{id:guid}")]
        public async Task<HttpStatusCode> EditEmployeeAsync([Required]Guid id, CustomerRequest customerRequest)
        {
            if (!ModelState.IsValid)
                return HttpStatusCode.BadRequest;
            
            var oldCustomer = await _customerRepository.GetByIdAsync(id);
            if (oldCustomer == null)
                return HttpStatusCode.NotFound;
            
            var preferences = await _preferenceRepository.GetAllAsync();
            var prefList = customerRequest.PreferenceIds.Select(prefGuid =>
                preferences.SingleOrDefault(x => x.Id == prefGuid)).ToList();

            oldCustomer.FirstName = customerRequest.FirstName;
            oldCustomer.LastName = customerRequest.LastName;
            oldCustomer.Email = customerRequest.Email;
            oldCustomer.Preferences = prefList;

            await _customerRepository.UpdateAsync(oldCustomer);
            
            return HttpStatusCode.Accepted;
        }
    }
}