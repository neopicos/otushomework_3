using System;

namespace Core.Domain
{
    /// <summary>
    /// Базовая сущность домена
    /// </summary>
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}