using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Клиент
    /// </summary>
    public class Customer : BaseEntity
    {
        /// <summary>
        /// Имя
        /// </summary>
        [MaxLength(255)]
        [Description("Имя")]
        public string FirstName { get; set; }
        
        /// <summary>
        /// Фамилия
        /// </summary>
        [MaxLength(255)]
        [Description("Фамилия")]
        public string LastName { get; set; }
        
        /// <summary>
        /// Инициалы (ФИ)
        /// </summary>
        [Description("Инициалы (ФИ)")]
        public string FullName => $"{FirstName} {LastName}";
        
        /// <summary>
        /// Email
        /// </summary>
        [MaxLength(300)]
        [Description("Email")]
        public string Email { get; set; }
        
        /// <summary>
        /// Предпочтения
        /// </summary>
        public virtual List<Preference> Preferences { get; set; }
        
        /// <summary>
        /// Промокоды
        /// </summary>
        public virtual List<PromoCode> PromoCodes { get; set; }
    }
}