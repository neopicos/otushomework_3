using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Предпочтение
    /// </summary>
    public class Preference : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(100)]
        [Description("Наименование")]
        public string Name { get; set; }
        
        /// <summary>
        /// Клиенты
        /// </summary>
        [Description("Клиенты")]
        public virtual List<Customer> Customers { get; set; }
    }
}