using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Core.Domain.Administration;

namespace Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Промокод
    /// </summary>
    public class PromoCode : BaseEntity
    {
        /// <summary>
        /// Код
        /// </summary>
        [MaxLength(40)]
        [Description("Код")]
        public string Code { get; set; }
        
        /// <summary>
        /// Описание
        /// </summary>
        [MaxLength(255)]
        [Description("Описание")]
        public string ServiceInfo { get; set; }
        
        /// <summary>
        /// Дата активации
        /// </summary>
        [Description("Дата активации")]
        public DateTime BeginDate { get; set; }
        
        /// <summary>
        /// Дата диактивации
        /// </summary>
        [Description("Дата диактивации")]
        public DateTime EndDate { get; set; }
        
        /// <summary>
        /// Наименование партнера
        /// </summary>
        [MaxLength(100)]
        [Description("Наименование партнера")]
        public string PartnerName { get; set; }
        
        /// <summary>
        /// Партнер
        /// </summary>
        [Description("Парнер")]
        public virtual Employee PartnerManager { get; set; }
        
        /// <summary>
        /// Соответствующее предпочтение
        /// </summary>
        [Description("Соответствующее предпочтение")]
        public virtual Preference Preference { get; set; }
        
        /// <summary>
        /// Клиент
        /// </summary>
        [MaxLength(100)]
        [Description("Клиент")]
        public virtual Customer Customer { get; set; }
    }
}