using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain.Administration
{
    /// <summary>
    /// Роль
    /// </summary>
    public class Role : BaseEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        [MaxLength(100)]
        [Description("Наименование")]
        public string Name { get; set; }
        
        /// <summary>
        /// Описание
        /// </summary>
        [MaxLength(250)]
        [Description("Описание")]
        public string Description { get; set; }
        
        /// <summary>
        /// Сотрудники
        /// </summary>
        public virtual List<Employee> Employees { get; set; }
    }
}